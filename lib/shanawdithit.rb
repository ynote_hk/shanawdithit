require "json"
require "active_support/core_ext/module/delegation"
require "./lib/shanawdithit/configuration"

# Core
require "./lib/shanawdithit/error/unfillable_tile_error"
require "./lib/shanawdithit/error/missing_tile_zoning_error"
require "./lib/shanawdithit/core/map"
require "./lib/shanawdithit/core/tile"
require "./lib/shanawdithit/core/collection"
require "./lib/shanawdithit/core/projection"

# Schema validator
require "./lib/shanawdithit/certificate/presence_validator"
require "./lib/shanawdithit/certificate/base_attestation"
require "./lib/shanawdithit/certificate/collection_attributes_attestation"
require "./lib/shanawdithit/certificate/base"

# Ascii projection
require "./lib/shanawdithit/projection/ascii/certificate"
require "./lib/shanawdithit/projection/ascii/tile_instrument"
require "./lib/shanawdithit/projection/ascii/base"

module Shanawdithit
  class << self
    def configuration
      @configuration ||= Configuration.new
    end

    def configure
      yield(configuration)
    end
  end
end
