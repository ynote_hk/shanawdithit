module Shanawdithit
  module Core
    class Tile
      def initialize(
        x:,
        y:,
        collection_area_types:,
        zoning: nil,
        tile_frame_file: Shanawdithit.configuration.tile_frame_file
      )
        @x = x
        @y = y
        @collection_area_types = collection_area_types
        @tile_frame_file = tile_frame_file
        @zoning = zoning || frame[:zoning]
      end

      attr_reader :x,
                  :y,
                  :zoning,
                  :collection_area_types,
                  :tile_frame_file

      def fill_zoning(boundaries = {})
        return zoning if zoning&.values&.compact&.count == 4

        fill_coerced_zones(boundaries) if boundaries.any?

        fill_empty_zones
      end

      def boundary_zones(orientation)
        return unless zoning&.any?

        orientation_zones = structure[orientation][:zones]

        orientation_zones.each_with_object({}) do |(_zone, attr), zones|
          zone_label = attr[:label].to_sym
          zones[zone_label] = zoning[zone_label]
        end
      end

      def label
        if unfilled?
          raise Error::MissingTileZoningError.new(
            "The tile is empty!",
            self,
          )
        end

        zoning.values.join("-")
      end

      def code
        if unfilled?
          raise Error::MissingTileZoningError.new(
            "The tile is empty!",
            self,
          )
        end

        code = zoning.values.map do |zone|
          collection_area_types[zone.to_sym][:code]
        end.join

        "tile-#{code}"
      end

      def unfilled?
        zoning.nil? || zoning.values.any?(&:nil?)
      end

      private

      attr_writer :zoning

      def fill_empty_zones
        self.zoning = zoning.to_h do |zone, area_type|
          area_type ? [zone, area_type] : [zone, random_area_type]
        end
      end

      # rubocop:disable Metrics/AbcSize
      def random_area_type
        # cf. https://gist.github.com/O-I/3e0654509dd8057b539a
        values = collection_area_types.values
        names = values.map { |type| type[:name] }
        default_weight = (1 / values.count.to_f).round(1)
        weights = values.map do |type|
          type[:weight] ? type[:weight].to_f : default_weight.to_f
        end
        weighted_names = names.zip(weights).to_h

        weighted_names.max_by do |_, weight|
          rand**(1.0 / weight)
        end.first
      end

      def fill_coerced_zones(boundaries = {})
        self.zoning = boundaries.each_with_object(
          zoning,
        ) do |(boundary_orientation, boundary_areas), new_zoning|
          next new_zoning unless boundary_areas&.values&.any?

          boundary_areas.each do |zone, area_type|
            zones = structure[boundary_orientation][:adjoining_zones]
            adjoining_zone = zones[zone][:matching].to_sym

            check_already_coerced_zone(area_type, adjoining_zone, zoning)

            new_zoning[adjoining_zone] = area_type
          end
        end
      end
      # rubocop:enable Metrics/AbcSize

      def check_already_coerced_zone(area_type, zone, zoning)
        return unless zoning[zone] && zoning[zone] != area_type

        raise Error::UnfillableTileError.new("The tile cannot be filled", self)
      end

      def frame
        @frame ||= begin
          file = File.read(tile_frame_file)
          JSON.parse(file, symbolize_names: true)
        end
      end

      def structure
        @structure || frame[:structure]
      end
    end
  end
end
