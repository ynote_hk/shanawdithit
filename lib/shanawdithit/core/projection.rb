module Shanawdithit
  module Core
    class Projection
      class << self
        def certify_with(classname)
          @certificate_class = classname
        end

        attr_reader :certificate_class
      end

      def initialize(
        map:,
        name:,
        title: Shanawdithit.configuration.title
      )
        check_certificate

        @map = map
        @name = name
        @title = title
      end

      attr_reader :map, :name, :title

      delegate :collection, to: :map

      def certificate
        return nil if self.class.certificate_class.nil?

        @certificate ||= self.class.certificate_class.new(
          collection: map.collection,
        )
      end

      private

      def check_certificate
        if self.class.certificate_class.nil? &&
           !instance_of?(Shanawdithit::Core::Projection)
          raise "Missing certificate for #{self.class}"
        end
      end
    end
  end
end
