module Shanawdithit
  module Core
    class Map
      def initialize(
        rows:,
        columns:,
        collection: Shanawdithit.configuration.collection
      )
        @total_rows = rows
        @total_columns = columns
        @collection = collection
      end

      attr_reader :total_columns, :total_rows, :collection

      def tiles
        @tiles ||= total_rows.times.map do |row_index|
          total_columns.times.map do |column_index|
            Tile.new(
              x: column_index,
              y: row_index,
              collection_area_types: collection.area_types,
            )
          end
        end
      end

      def populate
        tiles[0][0].fill_zoning

        tiles.each do |row|
          row.each do |tile|
            boundaries = tile_boundaries(tile)

            tile.fill_zoning(boundaries)
          end
        end
      end

      def add_projection(name:, projection:)
        new_projection = projection.new(
          map: self,
          name: projection_sanitized_name(name),
        )

        projections[name] = new_projection

        method_name = projection_sanitized_name(name)

        self.class.send(:define_method, method_name) do
          new_projection
        end

        new_projection
      end

      def projections
        @projections ||= {
          "default" => default_projection,
        }
      end

      def default_projection
        @default_projection ||=
          Object.const_get(collection.default_projection)
                .new(map: self, name: "default")
      end

      private

      attr_writer :tiles

      def projection_sanitized_name(name)
        name.downcase.strip.gsub(/[^0-9A-Za-z_]/, "").concat("_projection")
      end

      def tile_boundaries(tile)
        {
          N: north_boundary(tile),
          S: south_boundary(tile),
          E: east_boundary(tile),
          W: west_boundary(tile),
        }
      end

      def north_boundary(tile)
        return nil if tile.y == 0

        tile_on_north = tiles[tile.y - 1][tile.x]

        tile_on_north.boundary_zones(:S)
      end

      def south_boundary(tile)
        return nil if tile.y == total_rows - 1

        tile_on_south = tiles[tile.y + 1][tile.x]

        tile_on_south.boundary_zones(:N)
      end

      def west_boundary(tile)
        return nil if tile.x % total_columns == 0

        tile_on_west = tiles[tile.y][tile.x - 1]

        tile_on_west.boundary_zones(:E)
      end

      def east_boundary(tile)
        return nil if (tile.x + 1) % total_columns == 0

        tile_on_east = tiles[tile.y][tile.x + 1]

        tile_on_east.boundary_zones(:W)
      end
    end
  end
end
