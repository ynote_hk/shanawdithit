module Shanawdithit
  module Core
    class Collection
      def initialize(filepath:)
        @filepath = filepath
      end

      attr_reader :filepath

      def to_h
        @to_h ||= begin
          file = File.read(filepath)
          JSON.parse(file, symbolize_names: true)
        end
      end

      def name
        to_h[:name]
      end

      def default_projection
        to_h[:default_projection]
      end

      def attributes
        to_h[:attributes] || {}
      end

      def area_types
        to_h[:area_types] || {}
      end

      def author
        to_h[:author]
      end

      def tiles
        to_h[:tiles] || {}
      end
    end
  end
end
