module Shanawdithit
  module Error
    class MissingTileZoningError < StandardError
      attr_reader :tile

      def initialize(message, tile)
        super(message)

        @tile = tile
      end
    end
  end
end
