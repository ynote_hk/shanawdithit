module Shanawdithit
  module Certificate
    module CollectionAttributesAttestation
      include PresenceValidator

      private

      def validate_collection_attributes_presence
        attrs = self.class.send(:collection_mandatory_attributes)

        attrs.each do |attribute|
          validate_key_presence(attribute, root: collection.attributes)

          validate_value_presence(attribute, root: collection.attributes)

          validate_value_filling(attribute, root: collection.attributes)
        end
      end
    end
  end
end
