module Shanawdithit
  module Certificate
    module BaseAttestation
      include PresenceValidator

      private

      def validate_presence(key)
        validate_key_presence(key)
        validate_value_presence(key)
        validate_value_filling(key)
      end
    end
  end
end
