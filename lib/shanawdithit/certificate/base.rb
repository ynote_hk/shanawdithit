module Shanawdithit
  module Certificate
    class Base
      include BaseAttestation
      include CollectionAttributesAttestation

      class << self
        def attributes(*attrs)
          collection_mandatory_attributes.concat(attrs)
        end

        private

        def collection_mandatory_attributes
          @collection_mandatory_attributes ||= []
        end
      end

      def initialize(collection:)
        @collection = collection
      end

      attr_reader :collection

      def validate
        validate_presence(:name)
        validate_presence(:default_projection)
        validate_collection_attributes_presence
      end
    end
  end
end
