module Shanawdithit
  module Certificate
    module PresenceValidator
      def validate_key_presence(attribute, root: collection.to_h)
        raise "Missing attribute :#{attribute}" unless root.key?(attribute)
      end

      def validate_value_presence(attribute, root: collection.to_h)
        raise "Attribute :#{attribute} is nil" unless root[attribute]
      end

      def validate_value_filling(attribute, root: collection.to_h)
        return if root[attribute].is_a?(Numeric)

        raise "Attribute :#{attribute} is empty" if root[attribute].empty?
      end
    end
  end
end
