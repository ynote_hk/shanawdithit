module Shanawdithit
  module Projection
    module Ascii
      class Certificate < Certificate::Base
        attributes :lines_per_tile,
                   :characters_per_line
      end
    end
  end
end
