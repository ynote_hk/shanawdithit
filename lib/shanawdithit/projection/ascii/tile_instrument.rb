module Shanawdithit
  module Projection
    module Ascii
      class TileInstrument
        def initialize(collection:)
          @collection = collection
        end

        attr_reader :collection

        delegate :tiles, to: :collection

        def height
          collection.attributes[:lines_per_tile]
        end

        def output(tile)
          projection(tile)[:ascii_per_line].map do |line|
            "#{line}\n"
          end.join
        end

        def output_per_line(tile)
          projection(tile)[:ascii_per_line]
        end

        def log(tile)
          log = "--- <Tile - x: #{tile.x} y: #{tile.y}> ---\n\n"
          log += "   x: #{tile.x}\n"
          log += "   y: #{tile.y}\n"
          log += "   code: #{tile.code}\n"
          log += "   label: #{tile.label}\n"
          log += "   output:\n\n"

          log + output(tile)
        end

        def projection(tile)
          return default_projection unless tiles[tile.code.to_sym]

          # Note to myself: here we chose arbitrary the first
          # representation. We could add more representations of
          # a tile and randomly select one.
          tiles[tile.code.to_sym][0]
        end

        def projection_exists?(tile)
          projection(tile) && projection(tile)[:ascii_per_line]
        end

        def default_projection
          tiles[:"tile-default"][0]
        end
      end
    end
  end
end
