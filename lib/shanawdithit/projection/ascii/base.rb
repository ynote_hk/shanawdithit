module Shanawdithit
  module Projection
    module Ascii
      class Base < Core::Projection
        certify_with Ascii::Certificate

        def draw(with_debug: false)
          certificate.validate

          if unfilled_map?
            raise StandardError, "Map should be populated before drawing"
          end

          debug if with_debug

          puts header
          puts map_output
          puts footer
        end

        def map_output
          map.tiles.each_with_index.reduce("") do |memo, (row, _i)|
            tile_instrument.height.times.each do |index|
              memo += "|"
              memo += row.map do |tile|
                tile_instrument.output_per_line(tile)[index]
              end.join
              memo += "|\n"
            end

            memo
          end
        end

        def header
          "|#{line}|\n|#{double_line}|\n|#{empty_line}|\n"
        end

        def footer
          footer = "|#{empty_line}|\n|#{line}|\n"

          footer += "|#{centered("-- #{title} --")}|\n"
          footer += "|#{centered(author)}|\n"
          footer += "|#{double_line}|\n"

          footer
        end

        private

        def tile_instrument
          @tile_instrument ||= TileInstrument.new(collection: collection)
        end

        def unfilled_map?
          map.tiles.flatten.any?(&:unfilled?)
        end

        def debug
          map.tiles.flatten.each do |tile|
            puts
            puts tile_instrument.log(tile)
            puts
          end
        end

        def width
          width = collection.attributes[:characters_per_line]

          width.to_i * map.total_columns
        end

        def centered(text)
          padding_left_size = (width / 2) - (text.length / 2)
          padding_left = padding_left_size.times.map { |_| " " }.join
          padding_right_size =
            width - (text.length + padding_left_size)
          padding_right = padding_right_size.times.map { |_| " " }.join

          "#{padding_left}#{text}#{padding_right}"
        end

        def author
          author = collection.author

          "Tiles drawn by #{author}"
        end

        def double_line
          width.times.map { |_| "=" }.join
        end

        def line
          width.times.map { |_| "_" }.join
        end

        def empty_line
          width.times.map { |_| " " }.join
        end
      end
    end
  end
end
