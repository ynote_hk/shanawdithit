# Collection

A `collection` is a JSON file that lists all the elements needed for the
rendering of a map. A `collection` is represented by an instance of
[`Shanawdithit::Core::Collection`](/lib/shanawdithit/core/collection.rb).

## JSON file

The default structure looks like:
```json
{
  "name": "My Awesome Collection",
  "default_projection": "MyScope::Projection",
  "author": "Ynote_hk aka Fanny Cheung",
  "attributes": {
    "awesome_person": "Calvin",
    "awesome_animal": "Hobbes"
  },
  "area_types": {
    …
  },
  "tiles": {
    …
  }
}
```
- **default_projection**: the name of the default associated
  [`Projection`](/docs/projection.md).
- **author**: the name of the author of the map. Here, it refers to the
  person who creates the visual rendering of the map (a designer for
  instance)
- **attributes**: a hash listing the attributes of the collection.
- **area_types**: a hash listing the available area types of the map.
- **tiles**: a hash listing the availables tiles for the rendering.

The format of a collection is defined by a
[`Certificate`](/docs/certificate.md).

## Collection public instance methods

### filepath

Returns the filepath of the JSON.

### to_h

Returns the JSON content as an hash, with all keys as symbols.

### name

Returns the key `name` of the JSON.

### default_projection

Returns the value of the key `default_projection` in the JSON.

### attributes

Returns the value of the key `attributes` in the JSON (default: {})

### area_types

Returns the value of the key `area_types` in the JSON (default: {})
