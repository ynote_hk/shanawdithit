# Ascii projection

## Quick start

```ruby
require './lib/shanawdithit/map'
require './lib/shanawdithit/ascii_projection'

ascii_collection = Collection.new(filepath: collection_file)
map = Shanawdithit::Map.new(
  rows: 5,
  columns: 8,
  collection: ascii_collection
)

# Populate an map with empty locations for each tile
map.populate

# Use a projection to draw the map
projection = Shanawdithit::AsciiProjection.new(map)
projection.draw
```
