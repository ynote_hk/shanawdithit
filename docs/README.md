# Shanawdithit Documentation

## Models

- [Map](/docs/map.md)
- [Projection](/docs/projection.md)
- [Collection](/docs/collection.md)
- [Certificate](/docs/certificate.md)

## Data model

Shanawdithit is intended to be a composable library. It can use different
projections and collections to draw maps. It enables to split the responsability
of the rendering mechanism and the listing of the visual elements of the map.

A [`Map`](/docs/map.md) is always associated with a
[`collection`](/docs/collection.md). By default, a `Map` is associated with
[the ASCII collection](/data/ascii_default.json). A
[`Map`](/docs/map.md) can have several [`Projection`](/docs/projection.md), so
you can draw the same representation of a map with differents designs.

A [`Projection`](/docs/projection.md) is a class that exposes how to draw a
map out of the data of a [`collection`](/docs/collection.md). A
[`Projection`](/docs/projection.md) is always associated with a
[`collection`](/docs/collection.md).

A [`collection`](/docs/collection.md) is a JSON file that lists all
the available elements for a map. It is associated with a default
[`Projection`](/docs/projection.md), but it can be used with other
[`Projection`](/docs/projection.md) as long as it matches the requirements of
this latter.

