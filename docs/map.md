# Map

## Instance public methods

### new(rows:, columns:, collection: default)

Create a new instance of `Map`. The default `collection` is [`ASCII-001`](/data/ascii_default.json).

### total_columns

Returns the number of columns of the `Map`.

### total_rows

Returns the number of rows of the `Map`.

### collection

Returns the associated [`collection`](/docs/collection.md) of the `Map`.

### populate

Fills the map with its tiles to define the representation of the existing areas of the `Map`. The number of tiles is defined by [`total_rows`](#total_rows) and [`total_columns`](#total_columns)

### tiles

Returns all the tiles of a `Map`.

### add_projection(name:, projection:)

Add and returns a [`Projection`](/docs/projection.md).

The `name` argument defines the key in the [`projections`](#projections) method
under which the projection is stored.

The `projection` argument defines the [`Projection`](/docs/projection.md) class
you want to use.

This method also generates an instance method. The method name is:
- based on the
provided `name` without spaces and special characters (except `_`),
- in lowercase,
- suffixed with `_projection`.

```rb
map = Shanawdithit::Core::Map.new(
  rows: 5,
  columns: 8,
)
# => <Shanawdithit::Core::Map:0x000055948ddd56b8 …>

map.add_projection(
  name: "Alice in Wonderland!",
  projection: Shanawdithit::Projection::Ascii::Base,
)
# => <Shanawdithit::Projection::Ascii::Base:0x000055948dfefa70
#      @name="aliceinwonderland_projection"
#      …
#    >

map.aliceinwonderland_projection
# => <Shanawdithit::Projection::Ascii::Base:0x000055948dfefa70
#      @name="aliceinwonderland_projection"
#      …
#    >

map.add_projection(
  name: "the_cheshire_Cat",
  projection: Shanawdithit::Projection::Ascii::Base,
)
# => <Shanawdithit::Projection::Ascii::Base:0x000055948e04c338
#      @name="the_cheshire_cat_projection"
#      …
#    >

map.the_cheshire_cat_projection
# => <Shanawdithit::Projection::Ascii::Base:0x000055948e04c338
#      @name="the_cheshire_cat_projection"
#      …
#    >
```

### projections

Returns an hash with all the associated projections of a `Map`. By default, the
default [`Projection`](/docs/projection.md) of the associated
[`collection`](/docs/collection.md) is stored under the key `"default"`.

### default_projection

Returns the default [`Projection`](/docs/projection.md) of the associated
[`collection`](/docs/collection.md).
