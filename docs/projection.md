# Projection

In cartography, map projection is the term used to describe a broad set of
transformations employed to represent the two-dimensional curved surface of
a globe on a plane.

In this project, we use the term `Projection` to identify the entity which is
responsible for drawing the fictionnal map out of a set of data from a
[`collection`](/docs/collection.md).

- [Quick start](#quick-start)
- [Public instance methods](#public-instance-methods)
- [Helpers](#helpers)

## Quick start

A `Projection`:
- inherit from
[`Shanawdithit::Core::Projection`](/lib/shanawdithit/core/projection.rb),
- implements a `draw` method,
- is associated with a [`Certificate`](/docs/certificate.md).

Typically, you create a new `Projection` by using:

```ruby
# projection/my_awesome_projection.rb
module MyScope
  class Projection < Shanawdithit::Core::Projection
    certify_with MyAwesomeCertificate

    def draw
      …
    end
  end
end
```

Then, you reference it in your [`collection`](/docs/collection.md):
```json
# data/my_awesome_collection.json
{
  "name": "My Awesome Collection",
  "default_projection": "MyScope::Projection",
  …
}
```

And you can use it with your map:
```rb
# Create the collection
awesome_collection = Shanawdithit::Core::Collection.new(
  filepath: "data/my_awesome_collection.json",
)
map = Shanawdithit::Core::Map.new(
  rows: 5,
  columns: 8,
  collection: awesome_collection,
)

# Populate the map
map.populate

# The projection of the map is defined by the collection.
# Use the projection to draw your map.
projection = map.default_projection
projection.draw
```

## Public instance methods

### new(map:, name:, title: default)

Returns a new projection. It raises an error if
[`certify_with`](#certify_with) is not defined.

The `map` argument must be an instance of
[`Shanawdithit::Core::Map`](/lib/shanawdithit/core/map.rb).

The `name` argument defines the name you want for the `Projection`.

The `title` argument is optional. The default value is defined by
[`Shanawdithit.configuration.title`](/lib/shanawdithit/configuration.rb).

### name

Returns the name of the projection.

### title

Returns the title of the projection.

### draw

You can implement this method as you like as long as it returns an "output".

It can simply output characters on a terminal, like the
[`AsciiProjection`](/lib/shanawdithit/projection/ascii/base.rb).
But it can also create more
interesting output like an image file or a svg. I think we have a lot to
explore here :)

### certificate

Return an instance of the [`Certificate`](/docs/certificate.md) defined by the
helper [`certify_with`](#certify_with).

As the `draw` method will use a [`collection`](/docs/collection.md) for the
rendering, a `Projection` have to define the data structure of its associated
collection. We use a [`Certificate`](/docs/certificate.md) to validate
the structure of the JSON file of a collection

[Read the documentation about `Certificate`](/docs/certificate.md)

## Helpers

### certify_with

This method defines the [`Certificate`](/docs/certificate.md) class you use to
validate the schema of your collection.

```ruby
module MyScope
  class Projection < Core::Projection
    certify_with MyAwesomeCertificate

    …
  end
end
```
