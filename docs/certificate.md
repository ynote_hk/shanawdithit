# Certificate

A `Certificate` is a schema validator for the JSON file of a
[`collection`](/docs/collection.md).

## Quick start

A `Certificate` inherits from
[`Shanawdithit::Certificate::Base`](/lib/shanawdithit/certificate/base.rb).

Each [`Projection`](/docs/projection.md) has a
certificate to validate the collection to which it is associated. You create a
`Certificate` by using:

```rb
module MyScope
  class MyAwesomeCertificate < Certificate::Base
    attributes :awesome_person,
               :awesome_animal
  end
end
```

Then, you can use it with a [`collection`](/docs/collection.md):
```rb
collection = Collection.new(filepath: "path/to/file.json")
certificate = MyScope::MyAwesomeCertificate.new(
  collection: collection,
)

certificate.validate
```

If the JSON file structure doesn't match the schema described by the
certificate, it will raise an error.

Given this JSON file:

```json
{
  "name": "My Collection",
  "default_projection": "MyScope::Projection",
  "attributes": {
    "awesome_person": "Alice"
  }
}
```

The above certificate would raise an error because the `awesome_animal` key is
missing:

```rb
certificate.validate
# =>  Missing attribute :awesome_animal (RuntimeError)
```

## Helpers

### attributes

The `attributes` helper validates the presence of the attribute value in the
`attributes` key of the JSON file.

```rb
module MyScope
  class MyAwesomeCertificate < Certificate::Base
    attributes :awesome_person,
               :awesome_animal
  end
end
```

It raises an error if:
- the attribute doesn't exist,
- the value of the attribute is `nil`,
- the value of the attribute is `empty`.
