# Shanawdithit

Shanawdithit is a library that generates random maps from a tile collection.
The main inspiration for this funny project comes from the board game
[Carcassonne](https://en.wikipedia.org/wiki/Carcassonne_(board_game)).

For the moment, the library can only generate ASCII maps.

![shanawdithit](/uploads/243be03edfb55b89adda04fa2818f407/shanawdithit.gif)

## Summary

- [Pre-requisites](#pre-requisites)
- [Quick start](#quick-start)
- [Development](#development)
- [Documentation](#documentation)
- [Next steps](#next-steps)
- [About the name](#about-the-name)

## Pre-requisites
- Ruby >= 2.7.6
- Bundler >= 2.2.3

## Quick start

```
bundle install
```

Then:
```ruby
require './lib/shanawdithit'

# Create a map with the default ASCII collection
map = Shanawdithit::Core::Map.new(
  rows: 5,
  columns: 8,
)

# Populate an map with empty locations for each tile
map.populate

# Use a projection to draw the map
projection = map.default_projection
projection.draw
```

## Development

Run the tests:
```
bin/rspec
```

## Documentation

[Check out the documentation](/docs/README.md) 📖

## Next steps

- Add `area_types` helper in `Shanawdithit::Certificate::Base`
- Add `tile_attribute` helper in `Shanawdithit::Certificate::Base`
- Create a new `Projection` that outputs an HTML list of images.

## About the name

As I get older, I'm more and more upset by all the things around us that are
clearly impacted by patriarchy and colonialism, and yet accepted as the norm.
We are so used to a "white/straight/cisgender/valid man" world that we don't
notice that lots of us are not even represented in our daily environment.

For this project, I decided to pay tribute to
[Shanawdithit](https://en.wikipedia.org/wiki/Shanawdithit), the last known
living member of the Beothuk people (Canada) who is known for her
contributions to the historical understanding of
[Beothuk culture](https://en.wikipedia.org/wiki/Beothuk), including some maps.
I hope that mentionning her name will pique curiosity and encourage people to
read about the
[Beothuk genocide](https://vancouversun.com/news/local-news/beothuk-genocide-remembered-200-years-after-kidnapping-and-murder).
I want to raise awareness on how history and culture are always greatly
influenced by the people who convey them. Bias and lack of points of view can
create cultural resources that claims to be universal, but actually, are simply
just the voice of the powerful.
