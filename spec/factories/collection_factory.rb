FactoryBot.define do
  factory :collection, class: "Shanawdithit::Core::Collection" do
    skip_create

    filepath { "./spec/data/simple_collection.json" }

    initialize_with { new(filepath: filepath) }

    trait :ascii do
      filepath { "./spec/data/ascii_collection.json" }
    end
  end
end
