require "rspec"
require "pry"
require "factory_bot"
require "./lib/shanawdithit"

RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods
  config.example_status_persistence_file_path =
    File.join(__dir__, "../tmp/rspec_example_status_persistence.txt")

  config.before(:suite) do
    FactoryBot.find_definitions
  end

  Kernel.srand 1234
end
