require "./spec/helper"

RSpec.describe Shanawdithit::Core::Projection do
  subject(:projection) do
    described_class.new(
      map: map,
      name: "alice_projection",
      title: "Alice",
    )
  end

  let(:collection) { create :collection }
  let(:map) do
    Shanawdithit::Core::Map.new(
      rows: 2,
      columns: 3,
      collection: collection,
    )
  end

  describe "#collection" do
    it "returns the collection associated with the collection" do
      expect(projection.collection).to eq(collection)
    end
  end

  describe "#map" do
    it "returns the map associated with the collection" do
      expect(projection.map).to eq(map)
    end
  end

  describe "#title" do
    it "returns the passed title" do
      expect(projection.title).to eq("Alice")
    end
  end

  describe "#certificate" do
    it "returns nil" do
      expect(projection.certificate).to be_nil
    end
  end
end
