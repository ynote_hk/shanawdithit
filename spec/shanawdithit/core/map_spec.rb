require "./spec/helper"

RSpec.describe Shanawdithit::Core::Map do
  subject(:map) do
    described_class.new(
      rows: 3,
      columns: 5,
      collection: collection,
    )
  end

  let(:collection) { create :collection, :ascii }

  describe "#tiles" do
    let(:tile_class) do
      class_double(Shanawdithit::Core::Tile).as_stubbed_const
    end

    before { allow(tile_class).to receive(:new) }

    it "creates the tiles of the map once and returns them" do
      map.tiles

      expect(tile_class).to have_received(:new).exactly(15).times

      map.tiles

      expect(tile_class).to have_received(:new).exactly(15).times
    end
  end

  describe "#populate" do
    subject(:map) do
      described_class.new(
        rows: 1,
        columns: 2,
        collection: collection,
      )
    end

    let(:tile_00) do
      instance_double(
        Shanawdithit::Core::Tile,
        x: 0,
        y: 0,
        boundary_zones: {},
        fill_zoning: {},
      )
    end

    let(:tile_01) do
      instance_double(
        Shanawdithit::Core::Tile,
        x: 1,
        y: 0,
        boundary_zones: {},
        fill_zoning: {},
      )
    end

    # rubocop:disable RSpec/SubjectStub
    before do
      allow(map)
        .to receive(:tiles)
        .and_return([[tile_00, tile_01]])
    end
    # rubocop:enable RSpec/SubjectStub

    it "fills the zoning of each tile matching its boundary zones" do
      map.populate

      expect(tile_00).to have_received(:boundary_zones).at_least(1)
      expect(tile_01).to have_received(:boundary_zones).at_least(1)
      expect(tile_00)
        .to have_received(:fill_zoning).exactly(1)
        .with({ E: {}, N: nil, S: nil, W: nil })
      expect(tile_01)
        .to have_received(:fill_zoning).exactly(1)
        .with({ E: nil, N: nil, S: nil, W: {} })
    end
  end

  describe "#add_projection" do
    let(:projection_class) { Shanawdithit::Projection::Ascii::Base }

    it "add a projection to the map" do
      add_projection = map.add_projection(
        name: "Alice $_wondeérland",
        projection: projection_class,
      )

      expect(add_projection).to be_an_instance_of(projection_class)
      expect(add_projection.collection).to eq(collection)
      expect(map.alice_wonderland_projection).to eq(add_projection)
    end
  end

  describe "projections" do
    let(:projection_class) { Shanawdithit::Projection::Ascii::Base }

    before do
      map.add_projection(
        name: "Alice $_wondeérland",
        projection: projection_class,
      )
      map.add_projection(
        name: "Cheshire",
        projection: projection_class,
      )
    end

    it "returns all the added projections" do
      expect(map.projections.count).to eq(3)
      expect(map.projections.values).to all(be_an_instance_of(projection_class))
      expect(map.projections.keys).to eq(
        ["default", "Alice $_wondeérland", "Cheshire"],
      )
    end
  end
end
