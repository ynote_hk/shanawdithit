require "./spec/helper"

RSpec.describe Shanawdithit::Core::Collection do
  subject(:collection) do
    described_class.new(filepath: "example.json")
  end

  # rubocop:disable Naming/VariableNumber
  let(:collection_json) do
    JSON.generate(
      {
        default_projection: "Alice",
        author: "The MadHatter",
        attributes: {
          format: "image",
        },
        area_types: {
          sea: {},
          land: {},
        },
        tiles: {
          "tile-1111": [],
          "tile-1110": [],
        },
      },
    )
  end
  # rubocop:enable Naming/VariableNumber

  before do
    allow(File)
      .to receive(:read)
      .and_return(collection_json)
  end

  # rubocop:disable Naming/VariableNumber
  describe "#to_h" do
    it "returns the hash of the loaded collection" do
      expect(collection.to_h).to eq(
        {
          default_projection: "Alice",
          author: "The MadHatter",
          attributes: {
            format: "image",
          },
          area_types: {
            sea: {},
            land: {},
          },
          tiles: {
            "tile-1111": [],
            "tile-1110": [],
          },
        },
      )

      expect(File)
        .to have_received(:read)
        .with("example.json")
    end
  end
  # rubocop:enable Naming/VariableNumber

  describe "#attributes" do
    it "returns the attributes" do
      expect(collection.attributes).to eq({
                                            format: "image",
                                          })
    end
  end

  describe "#area_types" do
    it "returns the available area types of the collection" do
      expect(collection.area_types).to eq({
                                            sea: {},
                                            land: {},
                                          })
    end
  end

  describe "#default_projection" do
    it "return the class of the associated default projection" do
      expect(collection.default_projection).to eq("Alice")
    end
  end

  describe "#author" do
    it "return the name of the author" do
      expect(collection.author).to eq("The MadHatter")
    end
  end

  # rubocop:disable Naming/VariableNumber
  describe "#tiles" do
    it "returns the available tiles of the collection" do
      expect(collection.tiles).to eq({
                                       "tile-1111": [],
                                       "tile-1110": [],
                                     })
    end
  end
  # rubocop:enable Naming/VariableNumber
end
