require "./spec/helper"

RSpec.describe Shanawdithit::Core::Tile do
  let(:subject) do
    described_class.new(
      x: 2,
      y: 3,
      collection_area_types: collection.area_types,
    )
  end
  let(:collection) do
    Shanawdithit.configuration.collection
  end

  describe "#fill_zoning" do
    let(:boundaries) do
      {
        N: {
          SE: :land,
          SW: :sea,
        },
        W: {
          SE: :tree,
          NE: :sea,
        },
      }
    end

    it "returns a filled tile" do
      expect(subject.fill_zoning[:NW]).not_to be_nil
      expect(subject.fill_zoning[:NE]).not_to be_nil
      expect(subject.fill_zoning[:SW]).not_to be_nil
      expect(subject.fill_zoning[:SE]).not_to be_nil
    end

    context "with an already filled tile" do
      let(:subject) do
        described_class.new(
          x: 2,
          y: 3,
          collection_area_types: collection.area_types,
          zoning: {
            NW: :tree,
            NE: :tree,
            SW: :sea,
            SE: :land,
          },
        )
      end

      it "returns the same filled tile" do
        expect(subject.fill_zoning).to eq(
          {
            NW: :tree,
            NE: :tree,
            SW: :sea,
            SE: :land,
          },
        )
      end
    end

    context "with matching boundaries" do
      it "returns a filled tile" do
        expect(subject.fill_zoning(boundaries))
          .to include({
                        NE: :land,
                        NW: :sea,
                        SW: :tree,
                      })

        expect(subject.fill_zoning(boundaries)[:SE]).not_to be_nil
      end
    end

    context "when boundaries are not matching" do
      let(:boundaries) do
        {
          N: {
            SE: :sea,
            SW: :land,
          },
          W: {
            SE: :land,
            NE: :tree,
          },
        }
      end

      it "raises an error" do
        expect { subject.fill_zoning(boundaries) }.to(
          raise_error(Shanawdithit::Error::UnfillableTileError),
        )
      end
    end
  end

  describe "#boundary_zones" do
    it "returns nothing" do
      expect(subject.boundary_zones(:N)).to eq({ NE: nil, NW: nil })
    end

    context "when the zoning is filled" do
      let(:subject) do
        described_class.new(
          x: 2,
          y: 3,
          collection_area_types: collection.area_types,
          zoning: {
            NE: :sea,
            NW: :sea,
            SE: :land,
            SW: :tree,
          },
        )
      end
      let(:zoning) do
        {
          NE: :sea,
          NW: :sea,
          SE: :land,
          SW: :tree,
        }
      end

      it "returns the north and south zones of the tile" do
        expect(subject.boundary_zones(:N))
          .to eq({
                   NE: :sea,
                   NW: :sea,
                 })

        expect(subject.boundary_zones(:S))
          .to eq({
                   SE: :land,
                   SW: :tree,
                 })
      end
    end
  end

  describe "#label" do
    it "raises an error" do
      expect { subject.label }.to(
        raise_error(Shanawdithit::Error::MissingTileZoningError),
      )
    end

    context "with a filled zoning" do
      let(:subject) do
        described_class.new(
          x: 2,
          y: 3,
          collection_area_types: collection.area_types,
          zoning: zoning,
        )
      end
      let(:zoning) do
        {
          NW: :tree,
          NE: :tree,
          SW: :sea,
          SE: :land,
        }
      end

      it "returns the label of the tile depending on the zoning" do
        expect(subject.label).to eq("tree-tree-sea-land")
      end
    end
  end

  describe "#code" do
    it "raises an error" do
      expect { subject.label }.to(
        raise_error(Shanawdithit::Error::MissingTileZoningError),
      )
    end

    context "with a filled zoning" do
      let(:subject) do
        described_class.new(
          x: 2,
          y: 3,
          collection_area_types: collection.area_types,
          zoning: zoning,
        )
      end
      let(:zoning) do
        {
          NE: :land,
          NW: :land,
          SE: :sea,
          SW: :land,
        }
      end

      it "returns the code of the tile depending on the zoning" do
        expect(subject.code).to eq("tile-2212")
      end
    end
  end

  describe "#unfilled?" do
    it "returns true" do
      expect(subject).to be_unfilled
    end

    context "with a filled zoning" do
      let(:subject) do
        described_class.new(
          x: 2,
          y: 3,
          collection_area_types: collection.area_types,
          zoning: {
            NW: :land,
            NE: :land,
            SW: :sea,
            SE: :sea,
          },
        )
      end

      it "returns false" do
        expect(subject).not_to be_unfilled
      end
    end
  end
end
