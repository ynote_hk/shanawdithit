require "./spec/helper"

class TestCertificate < Shanawdithit::Certificate::Base
  attributes :best_character,
             :place
end

class MissingAttrCertificate < Shanawdithit::Certificate::Base
  attributes :best_character,
             :world
end

class NilAttrCertificate < Shanawdithit::Certificate::Base
  attributes :best_character,
             :tea
end

class EmptyAttrCertificate < Shanawdithit::Certificate::Base
  attributes :best_character,
             :friend
end

RSpec.describe Shanawdithit::Certificate::Base do
  subject(:certificate) { TestCertificate.new(collection: collection) }

  let(:collection) { create :collection }

  describe "#validate" do
    it "returns a truthy value" do
      expect(certificate.validate).to be_truthy
    end

    context "when an attribute is missing" do
      subject(:certificate) do
        MissingAttrCertificate.new(collection: collection)
      end

      it "raise an error" do
        expect { certificate.validate }
          .to raise_error(RuntimeError, "Missing attribute :world")
      end
    end

    context "when an attribute is nil" do
      subject(:certificate) do
        NilAttrCertificate.new(collection: collection)
      end

      it "raise an error" do
        expect { certificate.validate }
          .to raise_error(RuntimeError, "Attribute :tea is nil")
      end
    end

    context "when an attribute is empty" do
      subject(:certificate) do
        EmptyAttrCertificate.new(collection: collection)
      end

      it "raise an error" do
        expect { certificate.validate }
          .to raise_error(RuntimeError, "Attribute :friend is empty")
      end
    end

    context "when the name is missing" do
      let(:collection_hash) do
        {
          default_projection: "AliceProjection",
          author: "Lewis Carroll",
          attributes: {
            best_character: "The Mad Hatter",
            place: "Wonderland",
          },
          area_types: {},
          tiles: {},
        }
      end

      before do
        allow(collection)
          .to receive(:to_h)
          .and_return(collection_hash)
      end

      it "raise an error" do
        expect { certificate.validate }
          .to raise_error(RuntimeError, "Missing attribute :name")
      end
    end

    context "when the name is nil" do
      let(:collection_hash) do
        {
          name: nil,
          default_projection: "AliceProjection",
          author: "Lewis Carroll",
          attributes: {
            best_character: "The Mad Hatter",
            place: "Wonderland",
          },
          area_types: {},
          tiles: {},
        }
      end

      before do
        allow(collection)
          .to receive(:to_h)
          .and_return(collection_hash)
      end

      it "raise an error" do
        expect { certificate.validate }
          .to raise_error(RuntimeError, "Attribute :name is nil")
      end
    end

    context "when the name is empty" do
      let(:collection_hash) do
        {
          name: "",
          default_projection: "AliceProjection",
          author: "Lewis Carroll",
          attributes: {
            best_character: "The Mad Hatter",
            place: "Wonderland",
          },
          area_types: {},
          tiles: {},
        }
      end

      before do
        allow(collection)
          .to receive(:to_h)
          .and_return(collection_hash)
      end

      it "raise an error" do
        expect { certificate.validate }
          .to raise_error(RuntimeError, "Attribute :name is empty")
      end
    end

    context "when the default projection is missing" do
      let(:collection_hash) do
        {
          name: "Alice",
          author: "Lewis Carroll",
          attributes: {
            best_character: "The Mad Hatter",
            place: "Wonderland",
          },
          area_types: {},
          tiles: {},
        }
      end

      before do
        allow(collection)
          .to receive(:to_h)
          .and_return(collection_hash)
      end

      it "raise an error" do
        expect { certificate.validate }
          .to raise_error(RuntimeError, "Missing attribute :default_projection")
      end
    end

    context "when the default projection is nil" do
      let(:collection_hash) do
        {
          name: "Alice",
          default_projection: nil,
          author: "Lewis Carroll",
          attributes: {
            best_character: "The Mad Hatter",
            place: "Wonderland",
          },
          area_types: {},
          tiles: {},
        }
      end

      before do
        allow(collection)
          .to receive(:to_h)
          .and_return(collection_hash)
      end

      it "raise an error" do
        expect { certificate.validate }
          .to raise_error(RuntimeError, "Attribute :default_projection is nil")
      end
    end

    context "when the default projection is empty" do
      let(:collection_hash) do
        {
          name: "Alice",
          default_projection: "",
          author: "Lewis Carroll",
          attributes: {
            best_character: "The Mad Hatter",
            place: "Wonderland",
          },
          area_types: {},
          tiles: {},
        }
      end

      before do
        allow(collection)
          .to receive(:to_h)
          .and_return(collection_hash)
      end

      it "raise an error" do
        expect { certificate.validate }
          .to raise_error(
            RuntimeError,
            "Attribute :default_projection is empty",
          )
      end
    end
  end
end
